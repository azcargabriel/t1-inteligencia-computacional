%Funcion que entrega el numero de bin donde se encuentra x.
%(segun el parametro binsEdges)
function y = getBin(x, binsEdges)
    y = 0;
    for i = 1:size(binsEdges, 2)-1
        if binsEdges(1, i) <= x && x <= binsEdges(1, i + 1)
            y = i;
            return;
        end
    end
end

