function gauss_naive_bayes(trainingDB, testDB)    
    %Creamos dos matrices, con pulsares y no pulsares.
    trainingDBPositives = trainingDB(trainingDB(:,9)==1 , 1:8);
    trainingDBNegatives = trainingDB(trainingDB(:,9)==0 , 1:8);
    
    %Promedios de cada clase.
    uPos = zeros(1, 8);
    for i = 1:8
        uPos(1, i) = mean(trainingDBPositives(:, i));
    end
    
    uNeg = zeros(1, 8);
    for i = 1:8
        uNeg(1, i) = mean(trainingDBNegatives(:, i));
    end
    
    %Covarianza de cada clase.
    cPos = cov(trainingDBPositives(:, 1:8));
    cNeg = cov(trainingDBNegatives(:, 1:8));
    
    %------------------------------------------------------------------------
    %Clasificacion
    testDBPositives = testDB(testDB(:,9)==1 , :);
    testDBNegatives = testDB(testDB(:,9)==0 , :);

    %Nuestros omega a analizar.
    start = -30;
    finish = 30;
    partitions = 100;
    omega = linspace(start, finish, partitions);
    %Lo hacemos exponencial para empezar cerca de 0 y terminar en un numero
    %grande
    omega = arrayfun(@(x) exp(x), omega);
    
    %Tasa verdaderos positivos y tasa falsos positivos
    tvp = zeros(1, partitions);
    tfp = zeros(1, partitions);
    
    for z = 1:size(omega, 2)
        truePositives = 0;
        falsePositives = 0;
        for i = 1:size(testDB, 1)
            x = testDB(i, :);
            correctAnswer = x(9);
            x = x(1:8);

            %Calculamos el numerador y el denominador para la verosimilitud.
            nPos = exp((-1/2) * (x-uPos) * inv(cPos) * transpose(x-uPos));
            dPos = sqrt((2*pi)^8 * det(cPos));

            nNeg = exp((-1/2) * (x-uNeg) * inv(cNeg) * transpose(x-uNeg));
            dNeg = sqrt((2*pi)^8 * det(cNeg));

            %Verosimilitudes de x segun clase.
            vPosX = nPos / dPos;
            vNegX = nNeg / dNeg;

            if vPosX / vNegX >= omega(1, z) && correctAnswer == 1
                truePositives = truePositives + 1;
            elseif vPosX / vNegX >= omega(1, z) && correctAnswer == 0
                falsePositives = falsePositives + 1;
            end
        end

        tvp(1, z) = truePositives / size(testDBPositives, 1);
        tfp(1, z) = falsePositives / size(testDBNegatives, 1);

    end

    plot(tfp, tvp)
end