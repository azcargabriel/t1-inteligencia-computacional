function histogram_naive_bayes(trainingDB, testDB)
    binQuantity = 20;

    %Creamos dos matrices, con pulsares y no pulsares.
    c9 = trainingDB(1:end, 9);
    trainingDBPositives = trainingDB(trainingDB(:,9)==1 , 1:8);
    trainingDBNegatives = trainingDB(trainingDB(:,9)==0 , 1:8);

    %Calculamos las probabilidades de las clases.
    positives = sum(c9(:) == 1);
    negatives = sum(c9(:) == 0);
    pulsarProb = positives / (positives + negatives);
    noPulsarProb = negatives / (positives + negatives);

    %Tendremos una matriz con los diferentes valores.
    %Por ejemplo, posHists(1, :) sera el histograma de la clase 1
    %asi como posEdges(1, :) seran los rangos de los bins de esa misma clase,
    %y posHistsN(1, :) sera el histograma normalizado de la clase 1.
    posHists = zeros(8, binQuantity);
    posHistsN = zeros(8, binQuantity);
    posEdges = zeros(8, binQuantity + 1);

    negHists = zeros(8, binQuantity);
    negHistsN = zeros(8, binQuantity);
    negEdges = zeros(8, binQuantity + 1);

    for k = 1:8
        [pHN] = histcounts(trainingDBPositives(1:end, k), binQuantity, 'Normalization', 'probability');
        [nHN] = histcounts(trainingDBNegatives(1:end, k), binQuantity, 'Normalization', 'probability');
        [pH, pE] = histcounts(trainingDBPositives(1:end, k), binQuantity);
        [nH, nE] = histcounts(trainingDBNegatives(1:end, k), binQuantity);

        posHists(k, :) = pH;
        posHistsN(k, :) = pHN;
        negHists(k, :) = nH;
        negHistsN(k, :) = nHN;
        posEdges(k, :) = pE;
        negEdges(k, :) = nE;
    end

    %------------------------------------------------------------------------
    %Clasificacion.
    testDBPositives = testDB(testDB(:,9)==1 , :);
    testDBNegatives = testDB(testDB(:,9)==0 , :);

    %Nuestros omega a analizar.
    start = -30;
    finish = 30;
    partitions = 100;
    omega = linspace(start, finish, partitions);
    %Lo hacemos exponencial para empezar cerca de 0 y terminar en un numero
    %grande
    omega = arrayfun(@(x) exp(x), omega);

    %Tasa verdaderos positivos y tasa falsos positivos
    tvp = zeros(1, partitions);
    tfp = zeros(1, partitions);

    for z = 1:size(omega, 2)
        binIndexes = zeros(1, 8);
        truePositives = 0;
        falsePositives = 0;
        for i = 1:size(testDB, 1)
            x = testDB(i, :);

            %Calculamos la verosimilitud de x con la clase pulsar.
            for j = 1:8
                binIndexes(1, j) = getBin(x(j), posEdges(j, :));
            end
            vPosX = calculatePWithHistogram(binIndexes, posHistsN, pulsarProb);

            %Calculamos la verosimilitud de x con la clase no pulsar.
            for j = 1:8
                binIndexes(1, j) = getBin(x(j), negEdges(j, :));
            end
            vNegX = calculatePWithHistogram(binIndexes, negHistsN, noPulsarProb);

            correctAnswer = x(9);
            if vPosX / vNegX >= omega(1, z) && correctAnswer == 1
                truePositives = truePositives + 1;
            elseif vPosX / vNegX >= omega(1, z) && correctAnswer == 0
                falsePositives = falsePositives + 1;
            end
        end

        tvp(1, z) = truePositives / size(testDBPositives, 1);
        tfp(1, z) = falsePositives / size(testDBNegatives, 1);

    end
    
    plot(tfp, tvp)    
end