function tarea1(trainingDB, testDB)
    figure
    hold on
    histogram_naive_bayes(trainingDB, testDB);
    gauss_naive_bayes(trainingDB, testDB);
    t = title('Curvas ROC');
    t.FontSize = 14;
    l = legend('histogramas', 'gaussianas');
    l.FontSize = 14;
    xl = xlabel('FPR');
    xl.FontSize = 14;
    yl = ylabel('TPR');
    yl.FontSize = 14;
    hold off
end