function y = splitDatabase()
    %Script para crear una base de datos de entrenamiento y de prueba
    %representativas. Se debe correr hasta que encuentre una solucion y una vez
    %encontradas las bbdd trabajar con esas (no cambiarlas)
    filename = 'HTRU_2.csv';
    db = csvread(filename);

    %Para el conjunto de entrenamiento tendremos 80% de los datos
    %lo que da un total de 14318 muestras
    %Para el conjunto de pruebas tendremos un 20% de los datos
    %que corresponde a 3580 muestras

    %Sacare 3580 muestras al azar y el resto lo dejare en el conjunto
    %de entrenamiento

    indexes = randperm(17898);

    testDB = zeros(3580, 9);
    trainingDB = zeros(14318, 9);

    for ind = 1:3580
        testDB(ind, :) = db(indexes(ind), 1:end);
    end

    i = 1;
    for ind2 = 3581:17898
        trainingDB(i, :) = db(indexes(ind2), 1:end);
        i = i + 1;
    end

    %Revisamos los porcentajes de positivos y negativos en cada db
    testPos = 0;
    for ind = 1:3580
        if testDB(ind, 9) == 1
            testPos = testPos + 1;
        end
    end

    trainingPos = 0;
    for ind = 1:14318
        if trainingDB(ind, 9) == 1
            trainingPos = trainingPos + 1;
        end
    end

    %Deberian estar cerca del 0.09% para que sea valido
    percentTest = testPos / 3580;
    percentTraining = trainingPos / 14318;

    if 0.09 <= percentTest && percentTest <= 0.092 && 0.09 <= percentTraining && percentTraining <= 0.092
       %La solucion satisface
       csvwrite('testDB.csv', testDB);
       csvwrite('trainingDB.csv', trainingDB);
       y = true;
       return;
    end

    y = false;

end
