%Funcion que calcula la verosimilitud de x en la clase que representa el hists.
%binsIndexes es el indice del bin donde se encuentra x en cada una de sus clases.
%prob es P(C)
function y = calculatePWithHistogram(binsIndexes, hists, prob)
    y = prob;
    for i = 1:(size(binsIndexes, 2) - 1)
        if(binsIndexes(1, i) > 0)
            y = y * hists(i, binsIndexes(i));
        end
    end
end
